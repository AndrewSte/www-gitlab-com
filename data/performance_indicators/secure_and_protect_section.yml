- name: Secure and Protect, Secure - Section MAU, SMAU - Unique users who have used a Secure scanner
  base_path: "/handbook/product/secure-and-protect-section-performance-indicators/"
  definition: The number of unique users who have run one or more Secure scanners.
  target: Progressively increasing month-over-month
  org: Secure and Protect Section
  section: secureprotect
  stage: secure
  public: true
  pi_type: Section MAU, SMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track
  implementation:
    status: Instrumentation
    reasons:
    - Chart is for .com-only data while usage ping becomes more stable for on-premise.
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid
      Gold/Ultimate users.
    - Threat Insights data is not yet included in this count as it just moved from
      Defend.
  metric_name: user_unique_users_all_secure_scanners
  sisense_data:
    chart: 9282766
    dashboard: 707777
    embed: v2
  sisense_data_secondary:
    chart: 9926446
    dashboard: 707777
    embed: v2
- name: Secure:Static Analysis - GMAU - Users running Static Analysis jobs
  base_path: "/handbook/product/secure-and-protect-section-performance-indicators/"
  definition: The highest of the number of unique users who have run SAST or Secret Detection jobs.
  target: Progressively increasing month-over-month, >10%
  org: Secure and Protect Section
  section: secureprotect
  stage: secure
  group: static_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track
  implementation:
    status: Complete
    reasons:
    - Currently missing self-managed instrumentation, using estimated.
    - Can't confidently differentiate between Core and Ultimate users. [WIP exploration](https://app.periscopedata.com/app/gitlab/718481/Static-Analysis-Metrics---@tmccaslin?widget=9573161&udv=1090066)
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid Gold/Ultimate users.
    - Chart is for .com-only data while usage ping becomes more stable for on-premise.
    - Known [issue for timeouts](https://gitlab.com/gitlab-org/gitlab/-/issues/233610) causing some data to not report.
  lessons:
    learned:
      - Tracking correlation between in app clicks and upgrades is extremely difficult with existing product analytics system. Will have to work closely with Telemetry and Growth teams to successfully track SAST Core usage upgrades
      - SAST to Core continues to have no effect on downgrades thus far. [Upcoming core experience improvements](https://gitlab.com/groups/gitlab-org/-/epics/4388) should further increase adoption and usage rates.
      - Secret Detection to Core had no effect on downgrades thus far however it has not significantly driven Gold Adoption in the same way SAST To Core has. Customers want more from Secret Detection like [auto-remediation](https://gitlab.com/gitlab-org/gitlab/-/issues/10047), which we are actively considering for the roadmap and [potential tie in with Secrets Management](https://gitlab.com/gitlab-org/gitlab/-/issues/216276). 
      - Beginning to see leveling off of growth spike from SAST & Secret Detection to Core and inclusion in Auto DevOps. Future feature additions should keep growth going. More messaging and enablement coming soon.
      - We're likely still undercounting this as we use an exact match on SAST & Secret Detection job names rather than ilike fuzzy match which will catch more job names. This is being updated. 
  monthly_focus:
    goals:
      - Now that SAST & Secret Detection  is availible to all plan types, creating a better experience for non-Ultimate users to help them discover value and become interested in upgrading. [Improved MR experience](https://gitlab.com/groups/gitlab-org/-/epics/4388) & [Improved Configuration Experience](https://gitlab.com/gitlab-org/gitlab/-/issues/241377).
      - Delivering [MVC of SAST & Secret Detection Custom Rulesets](https://gitlab.com/groups/gitlab-org/-/epics/4179), our most requested feature, to drive paid adoption and extensibility of SAST. 
      - Major [rewrite of NodeJS SAST scanner](https://gitlab.com/gitlab-org/gitlab/-/issues/220847) to add 100+ new rules which will increase coverage and improve accuracy. 
      - Adding highly requested [iOS & Android SAST scanning](https://gitlab.com/gitlab-org/gitlab/-/issues/233777) to drive additional usage and adoption of the Secure stage. 
      - We're wrapping up our [MVC SAST Configuration UI](https://gitlab.com/groups/gitlab-org/-/epics/3262) which now supports existing configurations and specific analyzer settings to make it easier to get started with SAST.
      - Starting design phase for [Configuration UI for Secret Detection](https://gitlab.com/groups/gitlab-org/-/epics/4496) to simplify enablement and drive adoption. 
  metric_name: user_sast_jobs, user_secret_detection_jobs
  sisense_data:
    chart: 9284778
    dashboard: 707777
    embed: v2
  sisense_data_secondary:
    chart: 9926456
    dashboard: 707777
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Secure:Dynamic Analysis - GMAU - Users running DAST
  base_path: "/handbook/product/secure-and-protect-section-performance-indicators/"
  definition: Number of unique users who have run one or more DAST jobs.
  target: Progressively increasing month-over-month, >10% per-month
  org: Secure and Protect Section
  section: secureprotect
  stage: secure
  group: dynamic_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Steady monthly increase over the last two months
  implementation:
    status: Complete
    reasons:
    - Instrumentation in place.
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid
      Gold/Ultimate users.
    - Chart is for .com-only data while usage ping becomes more stable for on-premise.
  lessons:
    learned:
      - On-demand scans driving MAU increase to above where it was in May
      - On-demand scans significantly increased the number of projects DAST is used in
      - More profile options need to be introduced for on-demand DAST scans to have broad usage
  monthly_focus:
    goals:
      - Add more options to Scanner profile (https://gitlab.com/gitlab-org/gitlab/-/issues/225804)
      - Add more options to Site profile (https://gitlab.com/groups/gitlab-org/-/epics/3771)
      - DAST Site validation (https://gitlab.com/gitlab-org/gitlab/-/issues/233020)
  metric_name: user_dast_jobs
  sisense_data:
    chart: 9620496
    dashboard: 697792
    embed: v2
  sisense_data_secondary:
    chart: 9926656
    dashboard: 707777
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Secure:Composition Analysis - GMAU - Users running any SCA scanners
  base_path: "/handbook/product/secure-and-protect-section-performance-indicators/"
  definition: The highest of the number of unique users who have run one or more Dependency Scanning jobs, or  number of unique users who have run one or more License Scanning jobs.
  target: Progressively increasing month-over-month, >2%
  org: Secure and Protect Section
  section: secureprotect
  stage: secure
  group: composition_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track
  implementation:
    status: Complete
    reasons:
    - Based on discussion with Anoop, who will be documenting all MVC shared understandings, this MVC is as expected and will be shared as a trend and not as ACTUAL numbers and there will be a single source of truth to maintain for the known current accepted data limitations and assumptions.
  lessons:
    learned:
      - All my work on [my dashboard](https://app.periscopedata.com/app/gitlab/749790/Secure-Software-Composition-Analysis-Dashboard) will be replaced by new work by the data team. See [this dashboard](https://app.periscopedata.com/app/gitlab/758607/WIP-SMAU-GMAU-Mathieu).
  monthly_focus:
    goals:
      - October: continue to update the DBT and work to improve the data teams documentation, as well as design and them implement a new flattening algorithm.
      - When there is time i would like to review in depth with engineering where in the cycle we trigger the count and update known caveats if needed. 
      - When there is time I would like to review with data or engineering all transformations that occur when ping is processed to send.
      - When there is time I would like to review with data all transformations that occur after ping is sent and review in case we want to make adjustments.
      - Hopefully creating a dashboard for my [SaaS PI - number of findings](https://gitlab.com/groups/gitlab-org/-/epics/3952).
  metric_name: user_license_management_jobs, user_dependency_scanning_jobs
  sisense_data:
    chart: 9822707
    dashboard: 749790
    embed: v2
  sisense_data_secondary:
    chart: 9926775
    dashboard: 707777
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Secure:Fuzz Testing - GMAU - Users running fuzz testing
  base_path: "/handbook/product/secure-and-protect-section-performance-indicators/"
  definition: The highest of the number of unique users who have run one or more Fuzz
    Testing jobs.
  target: Progressively increasing month-over-month, >10% per-month
  org: Secure and Protect Section
  section: secureprotect
  stage: secure
  group: fuzz_testing
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - We have a good growth rate and need more users to use fuzz testing.
  implementation: 
    status: Complete
    reasons:
    - We now have reporting on .com for both coverage and API fuzz testing.
    - Self-managed likely to capture no results due to being keyed off of job name rather than job type. Tracking work on improving this in [an issue](https://gitlab.com/gitlab-org/gitlab/-/issues/239118). We _can_ see the number of jobs, just not users.
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid Gold/Ultimate users.
  lessons:
    learned:
      - We learned how to use a new source for reporting .com usage until we can report on job artifacts. This data appears accurate so may not need to move off it for .com.
      - We learned about changes needed to usage ping. Since users can set their own job names, we can't look just at job names.
      - We observed .com Snowplow working correctly, as we had several users reporting data.
      - Self-managed likely to capture no results due to being keyed off of job name rather than job type. Tracking work on improving this in [an issue](https://gitlab.com/gitlab-org/gitlab/-/issues/239118).
  monthly_focus:
    goals:
      - Fuzz testing results in MR widget (https://gitlab.com/gitlab-org/gitlab/-/issues/210343)
      - Investigation for how to support Java Spring apps (https://gitlab.com/gitlab-org/gitlab/-/issues/254654)
      - API fuzz testing foundational work to use Security Dashboard (https://gitlab.com/gitlab-org/gitlab/-/issues/224726)
      - Re-symbolicate fuzz crash results (https://gitlab.com/gitlab-org/gitlab/-/issues/224518)
  metric_name: user_coverage_fuzzing_jobs on self-managed, (options like '%gitlab-cov-fuzz%' OR options like '%FUZZAPI_REPORT%') on .com
  sisense_data:
    chart: 9842394
    dashboard: 707777
    embed: v2
  sisense_data_secondary:
    chart: 9926832
    dashboard: 707777
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Secure:Threat Insights - GMAU - Users interacting with Secure UI
  base_path: "/handbook/product/secure-and-protect-section-performance-indicators/"
  definition: Number of unique sessions viewing a security dashboard, pipeline security
    report, or expanding MR security report. Same as Paid until OSS projects
    can be separated.
  target: Progressively increasing month-over-month, >10% per-month
  org: Secure and Protect Section
  section: secureprotect
  stage: secure
  group: threat_insights
  public: true
  pi_type: GMAU
  product_analytics_type: SaaS
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - Data numbers and trends may be misrepresented (see below)
  implementation:
    status: Instrumentation
    reasons:
    - Recent deep-dive into data indicates page view usage queries may not be accurate
    - GitLab employee usage may be greatly overcounted for certain feature pages
    - May require new data instrumentation to correct
    - Collection is complete for .com only
    - Snowplow data is session only, not unique users
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid
      Gold/Ultimate users.
    - Large Snowplow data loss event in September and October obscure true performance
    - Collection is complete for .com only
    - Snowplow data is session only, not unique users
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid
      Gold/Ultimate users.
    - Cannot separate out GitLab employee usage from customers
  lessons:
    learned:
      - Page URL pattern matching may be insufficiently deterministic to support accurate GMAU
      - N/A: Data integrity problems make it impossible to make accurate performance and impact assessments
  monthly_focus:
    goals:
      - Work with Data team to resolve current data issues and/or formulate new metrics capture plan: https://gitlab.com/gitlab-data/analytics/-/issues/6404
      - Continue executing on roadmap based on input from other sensing mechanisms
  metric_name: n/a
  sisense_data:
    chart: 9244137
    dashboard: 707777
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Protect, Protect:Container Security - SMAU, GMAU - Users running Container Scanning or interacting with Protect UI
  base_path: "/handbook/product/secure-and-protect-section-performance-indicators/"
  definition: The number of active clusters with at least one Container Security feature
    enabled (WAF, Network Policies, or Host Security) in the last 28 days.
  target: Progressively increasing month-over-month, >2%
  org: Secure and Protect Section
  section: secureprotect
  stage: Protect
  group: container_security
  public: true
  pi_type: SMAU, GMAU
  product_analytics_type: Both
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track
  implementation:
    status: Complete
    reasons:
    - Limitations in data (see SCA limitations above)
    - Data is not available yet for the Security Orchestration category as it has not yet reached Minimal maturity
  lessons:
    learned:
      - TBD
  monthly_focus:
    goals:
      - Allow container security scans to be run against containers in production environments (https://gitlab.com/groups/gitlab-org/-/epics/3410)
      - Project-level DAST Scan Schedule Policies (https://gitlab.com/groups/gitlab-org/-/epics/4598)
  metric_name: user_container_scanning_jobs
  sisense_data:
    chart: 9930861
    dashboard: 694854
    embed: v2
- name: Protect, Protect:Container Security - SMAC, GMAC - Clusters using Container Network or Host Security
  base_path: "/handbook/product/secure-and-protect-section-performance-indicators/"
  definition: The number of active clusters with at least one Container Security feature
    enabled (Network Policies, or Host Security) in the last 28 days.
  target: Progressively increasing >25% month-over-month
  org: Secure and Protect Section
  section: secureprotect
  stage: Protect
  group: container_security
  public: true
  pi_type: SMAC, GMAC
  product_analytics_type: Both
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track.  Usage grew from 7 clusters to 16 from September to October.
  implementation:
    status: Complete
    reasons:
    - Data collection for CHS is [planned but not yet implemented](https://gitlab.com/gitlab-org/gitlab/-/issues/218800) as the category is relatively new
  lessons:
    learned:
      - We have 16 clusters using CNS, all of which are on gitlab.com.  Customers are using the blocking mode as a significant amount of traffic is being dropped.
  monthly_focus:
    goals:
      - Alert Dashboard MVC (https://gitlab.com/groups/gitlab-org/-/epics/3438)
  metric_name: TBD
  sisense_data:
    chart: 9620410
    dashboard: 694854
    embed: v2
 
