---
layout: markdown_page
title: "People Group Roles"
---

For an overview of all people ops roles please see the [People Group](https://about.gitlab.com/handbook/people-group/) [roles directory in the repository](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/sites/marketing/source/job-families/people-ops).
